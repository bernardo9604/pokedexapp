# pokemon-app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Librerias usadas

```
# Axios: Para las peticiones http (API)
# Sweet Alert 2:  Se uso basicamente para las alertas al usuario
# Vuex: Manejo del Store
# eslint: Manejo de errores dentro de la aplicación

# Nota: "Trate de usar las menores librerias posibles, dado que no se especificaba si se deberian usar."

````

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


