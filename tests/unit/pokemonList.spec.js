import PokemonList from "../../src/components/pokemon/PokemonList";
import {mount} from "@vue/test-utils"

it("Should mount the component and contains a pokemon", () => {
    const pokemonName = "ivysaur"
    const pokemonList = mount(PokemonList, {
        propsData: {
            pokemon: {
                name: pokemonName
            }
        }
    })
    expect(pokemonList.text()).toContain(pokemonName)
})
