import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        pokemons: [],
        favoritePokemons: [],
        pokemonName: "",
        loading: false,
        limite: 30,
    },
    mutations: {
        SET_POKEMONS(state, payload) {
            state.pokemons = payload;
        },
        SET_FAVORITES_POKEMONS(state, payload) {
            state.favoritePokemons.push(payload);
        },
        SET_FAVORITE_POKEMON(state, payload) {
            state.findFavoritePokemon = payload;
        },
        SET_LOADING(state, payload) {
            state.loading = payload;
        },
        ADD_POKEMON_NAME(state, payload) {
            state.pokemonName = payload;
        },
        CHECK_POKEMON_TRUE(state, payload) {
            const index = state.pokemons.findIndex(
                (pokemon) => pokemon.name.toLowerCase() === payload.name.toLowerCase()
            );
            state.pokemons[index].check = true;
        },
        CHECK_POKEMON_FALSE(state, payload) {
            const index = state.pokemons.findIndex(
                (pokemon) => pokemon.name.toLowerCase() === payload.name.toLowerCase()
            );
            state.pokemons[index].check = false;
        },
        DELETE_FAVORITE_POKEMON(state, payload) {
            let index = state.favoritePokemons.findIndex(
                (pokemon) =>
                    pokemon.name.toLowerCase().trim() ===
                    payload.name.toLowerCase().trim()
            );
            state.favoritePokemons.splice(index, 1);
        },
    },
    actions: {
        async getPokemons({commit, state}) {
            commit("SET_LOADING", true);
            try {
                const response = await axios.get(
                    `https://pokeapi.co/api/v2/pokemon?limit=${state.limite}`
                );
                const pokemons = response.data.results.map((item) => {
                    let pokemonFind = state.favoritePokemons.find(
                        (fp) => fp.name === item.name
                    );
                    return {...item, check: pokemonFind !== undefined};
                });
                commit("SET_POKEMONS", pokemons);
                setTimeout(() => commit("SET_LOADING", false), 1000);
            } catch (e) {
                console.log(e);
                commit("SET_LOADING", false);
            }
        },
        async getPokemonByName({commit, state}, payload) {
            commit("SET_LOADING", true);
            try {
                const response = await axios.get(
                    `https://pokeapi.co/api/v2/pokemon/${payload}?limit=${state.limite}`
                );
                let pokemonFind = state.favoritePokemons.find(
                    (fp) => fp.name === response.data.name
                );
                commit("SET_POKEMONS", [
                    {...response.data, check: pokemonFind !== undefined},
                ]);
                setTimeout(() => commit("SET_LOADING", false), 1000);
            } catch (e) {
                console.log(e);
                commit("SET_POKEMONS", []);
                setTimeout(() => commit("SET_LOADING", false), 1000);
            }
        },
    },
    getters: {
        pokemons: (state) => state.pokemons,
        favoritePokemons: (state) => {
            if (state.pokemonName !== "")
                return state.favoritePokemons.filter((pk) => {
                    return (
                        pk.name
                            .toLowerCase()
                            .indexOf(state.pokemonName.toLowerCase().trim()) > -1
                    );
                });
            return state.favoritePokemons;
        },
        loading: (state) => state.loading,
    },
    modules: {},
});
