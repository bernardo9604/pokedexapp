import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/pokemons",
    name: "pokemons",
    component: () => import("../views/Pokemons.vue"),
  },
  {
    path: "/favorite-pokemons",
    name: "favoritePokemons",
    component: () => import("../views/FavoritePokemons.vue"),
  },
  {
    path: "/error-page",
    name: "errorPage",
    component: () => import("../views/ErrorPage.vue"),
  },
];

const router = new VueRouter({
  routes,
});

router.beforeEach((to, from, next) => {
  if(to.name === null)
   return next({name: "errorPage"})
  else return next()
})

export default router;
