import store from "../store";

const pokemonFinder = (name) => {
  store.state.favoritePokemons.find(
    (pk) => pk.name.toLowerCase() === name.toLowerCase()
  );
};

export { pokemonFinder };
