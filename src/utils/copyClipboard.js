export function copyToClipBoard(text) {
  try {
    navigator.clipboard.writeText(text);
  } catch (e) {
    console.log(e);
    throw e;
  }
}
