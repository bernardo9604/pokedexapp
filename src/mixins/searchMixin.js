export default {
    methods: {
        searchPokemon(name) {
            if (name !== "")
                this.$store.dispatch("getPokemonByName", name.toLowerCase());
            else this.$store.dispatch("getPokemons");
        },
    },
};
