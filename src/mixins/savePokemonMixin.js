export default {
    methods: {
        savePokemon(pokemon) {
            if (pokemon.check === true) {
                this.$store.commit("CHECK_POKEMON_FALSE", pokemon);
                this.$store.commit("DELETE_FAVORITE_POKEMON", pokemon);
            } else {
                this.$store.commit("CHECK_POKEMON_TRUE", pokemon);
                this.$store.commit("SET_FAVORITES_POKEMONS", pokemon);
                this.$swal("Success", `Pokemon "${pokemon.name.toUpperCase()}" added to your favorites...`, "success")
            }
        },
    },
};
